import { Component, OnInit } from '@angular/core';
import { ProductService } from './../shared/product.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  Products: any = [];

  constructor(
    private productService: ProductService
  ) {
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.productService.getProductList().subscribe((res) => {
      console.log(res);
      this.Products = res;
    });
  }

  deleteProduct(product, i) {
    if (window.confirm('Do you want to delete the product?')) {
      this.productService.deleteProduct(product.id)
        .subscribe(() => {
          this.Products.splice(i, 1);
          console.log('product deleted!');
        }
        );
    }
  }
}
