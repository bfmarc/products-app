import { Component, OnInit } from '@angular/core';
import { ProductService } from './../shared/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NumberValidator } from './../validators/number';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.page.html',
  styleUrls: ['./edit-product.page.scss'],
})
export class EditProductPage implements OnInit {

    updateProductForm: FormGroup;
    id: any;

    constructor(
      private productAPI: ProductService,
      private actRoute: ActivatedRoute,
      private router: Router,
      public fb: FormBuilder
    ) {
      this.id = this.actRoute.snapshot.paramMap.get('id');
    }

    ngOnInit() {
      this.getProductData(this.id);
      this.updateProductForm = this.fb.group({
        barcode: ['', [Validators.required, Validators.maxLength(250)]],
        product_name: ['', [Validators.required, Validators.maxLength(250)]],
        price: ['', [Validators.required, NumberValidator.isValid]],
        stock: ['', [Validators.required, NumberValidator.isValid]]
      });
    }

    getProductData(id) {
      this.productAPI.getProduct(id).subscribe(res => {
        this.updateProductForm.setValue({
          barcode: res['barcode'],
          product_name: res['product_name'],
          price: res['price'],
          stock: res['stock']
        });
      });
    }

    updateForm() {
      if (!this.updateProductForm.valid) {
        return false;
      } else {
        this.productAPI.updateProduct(this.id, this.updateProductForm.value)
          .subscribe((res) => {
            console.log(res);
            this.updateProductForm.reset();
            this.router.navigate(['/home']);
          });
      }
    }
}
