import { Injectable } from '@angular/core';
import { Product } from './product';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApplicationConfig } from './../app-config';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  addProduct(product: Product): Observable<any> {
    return this.http.post<Product>(ApplicationConfig.apiEndpoint, product, this.httpOptions)
      .pipe(
        catchError(this.handleError<Product>('Add product'))
      );
  }

  getProduct(id): Observable<Product[]> {
    return this.http.get<Product[]>(ApplicationConfig.apiEndpoint + id)
      .pipe(
        tap(_ => console.log(`Product fetched: ${id}`)),
        catchError(this.handleError<Product[]>(`Get product id=${id}`))
      );
  }

  getProductList(): Observable<Product[]> {
    return this.http.get<Product[]>(ApplicationConfig.apiEndpoint)
      .pipe(
        tap(products => console.log('Products fetched!')),
        catchError(this.handleError<Product[]>('Get Products', []))
      );
  }

  updateProduct(id, product: Product): Observable<any> {
    return this.http.put(ApplicationConfig.apiEndpoint + id, product, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Product updated: ${id}`)),
        catchError(this.handleError<Product[]>('Update Product'))
      );
  }

  deleteProduct(id): Observable<Product[]> {
    return this.http.delete<Product[]>(ApplicationConfig.apiEndpoint + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Product deleted: ${id}`)),
        catchError(this.handleError<Product[]>('Delete Product'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
