export class Product {
    _id: number;
    barcode: string;
    product_name: string;
    price: number;
    stock: number;
}
