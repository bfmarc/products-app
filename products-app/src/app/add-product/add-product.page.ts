import { Component, OnInit, NgZone } from '@angular/core';
import { ProductService } from './../shared/product.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NumberValidator } from './../validators/number';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {
    productForm: FormGroup;
    isSubmitted = false;

    constructor(
      private productAPI: ProductService,
      private router: Router,
      public fb: FormBuilder,
      private zone: NgZone
    ) {
      this.productForm = this.fb.group({
        barcode: ['', [Validators.required, Validators.maxLength(250)]],
        product_name: ['', [Validators.required, Validators.maxLength(250)]],
        price: ['', [Validators.required, NumberValidator.isValid]],
        stock: ['', [Validators.required, NumberValidator.isValid]]
      });
    }

    ngOnInit() { }

    get errorControl() {
      return this.productForm.controls;
    }

    onFormSubmit() {
      this.isSubmitted = true;
      if (!this.productForm.valid) {
        console.log('Please provide all the required values!');
        return false;
      } else {
        this.productAPI.addProduct(this.productForm.value)
          .subscribe((res) => {
            this.zone.run(() => {
              console.log(res);
              this.productForm.reset();
              this.router.navigate(['/home']);
            });
          });
      }
    }
}
