// Configuration values for our app
export const ApplicationConfig = {
  appName: 'Products management demo',
  apiEndpoint: 'http://127.0.0.1:8000/api/products/'
};
