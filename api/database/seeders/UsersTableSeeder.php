<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = Factory::create();

        $password = Hash::make('password');

        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.lu',
            'password' => $password,
        ]);
    }
}
