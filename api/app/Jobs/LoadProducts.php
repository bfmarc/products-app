<?php

namespace App\Jobs;

use App\Models\Product;
use ErrorException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

/**
 * Job to import products from an import file
 * located in the storage/import folder
 *
 * @var string
 */
class LoadProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $filePath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (! Storage::disk('import')->exists(config('app.import_products_file'))) {
          throw new ErrorException('Import product file not found');
        }
        $this->filePath = Storage::disk('import')->path(config('app.import_products_file'));
    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {
      print('Importing products' . ' \r\n ');
      $i = 1;
      foreach ($this->readFile() as $line) {
        $data = explode(';', $line);
        print('Insert product : ' . $data[0] . ' \r\n ');
        if (count($data) > 6) {
          Product::create([
              'barcode' => $data[0],
              'product_name' => $data[1],
              'price' => $data[5],
              'stock' => $data[6],
          ]);
          $i ++;
        }
      }
      print($i . ' products imported successfully' . ' \r\n ');
    }

    /**
     * Reading the file line by line with yield function
     *
     * @return string
     */
    private function readFile() {
        $file = fopen($this->filePath, 'r');

        while (($line = fgets($file)) !== false) {
            yield $line;
        }

        fclose($file);
    }
}
