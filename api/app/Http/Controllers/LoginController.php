<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

/**
 * Login controller
 *
 */
class LoginController extends Controller
{
  /**
   * Login
   *
   * @return response
   */
  public function login(Request $request)
  {
      $request->validate([
          'email' => 'required',
          'password' => 'required',
      ]);

      $credentials = $request->only('email', 'password');
      if (Auth::attempt($credentials)) {
        $user = Auth::guard('api')->user();
        $user->generateToken();

        return response()->json([
            'data' => $user->toArray(),
        ]);
      }

      $errors = [ 'error' => trans('Mauvais identifiant') ];

      return response()->json($errors, 422);
  }

  /**
   * logout
   *
   * @return response
   */
  public function logout() {
    $user = Auth::guard('api')->user();

    if ($user) {
        $user->api_token = null;
        $user->save();
    }

    Session::flush();
    Auth::logout();

    return response()->json(['data' => 'User logged out.'], 200);
  }

  /**
   * Unauthenticated
   *
   * @return response
   */
  protected function unauthenticated($request, AuthenticationException $exception)
  {
      return response()->json(['error' => 'Unauthenticated'], 401);
  }
}
