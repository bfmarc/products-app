<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

/**
 * Product controller
 *
 * @return void
 */
class ProductController extends Controller
{
  /**
   * Get the list of products
   *
   * @return array
   */
  public function index()
  {
      return Product::all();
  }

  /**
   * Get a product
   *
   * @return Product
   */
  public function show(Product $product)
  {
      return $product;
  }

  /**
   * Inserting a product
   *
   * @return response
   */
  public function store(Request $request)
    {
      $request->validate([
          'barcode' => 'required',
          'product_name' => 'required',
            'price' => 'required',
            'stock' => 'required',
      ]);

      $data = $request->only('barcode', 'product_name', 'price','stock');
      $product = Product::create($request->all());
      return response()->json($product, 201);
  }

  /**
   * Updating a product
   *
   * @return response
   */
  public function update(Request $request, Product $product)
  {
    $request->validate([
        'barcode' => 'required',
        'product_name' => 'required',
          'price' => 'required',
          'stock' => 'required',
    ]);

    $data = $request->only('barcode', 'product_name', 'price','stock');
    $product->update($data);
    return response()->json($product, 200);
  }

  /**
   * Deleting a product
   *
   * @return response
   */
  public function delete(Request $request, Product $product)
  {
    $product->delete();
    return response()->json(null, 204);
  }
}
